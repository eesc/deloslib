# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.contrib.auth.models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0005_auto_20150305_0854'),
    ]

    operations = [
        migrations.AlterModelManagers(
            name='useraccount',
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.AlterField(
            model_name='contact',
            name='mail_from',
            field=models.EmailField(max_length=254, verbose_name='Email'),
        ),
        migrations.AlterField(
            model_name='role',
            name='role',
            field=models.CharField(max_length=1, verbose_name='Papel', choices=[(b'A', 'Administrador'), (b'C', 'Convidado'), (b'T', 'T\xe9cnico'), (b'O', 'Orientador'), (b'S', 'Secret\xe1ria')]),
        ),
        migrations.AlterField(
            model_name='useraccount',
            name='last_login',
            field=models.DateTimeField(null=True, verbose_name='last login', blank=True),
        ),
    ]
