## About ##

This Django module is part of Delos system, and is intended to support users management with Roles and "Unidade".

Desloslib está sendo usado pelo delos server que gerencia notas fiscais da EESC e também no SET nos sistemas locais.

## Installation ##

To install all deps:

```sh
pip install -r requirements.txt
```

```python

```

## License ##

Delos-users is BSD licensed.
